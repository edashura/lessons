# Lessons

`Lessons` is the git repository holding the content material for the UL Devops courses @ Polytech Nancy.

# Table of content

* [Session 1][session-1]
* [Session 2][session-2]

[session-1]: ./session-1.md
[session-2]: ./session-2.md
